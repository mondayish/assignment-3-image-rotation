#ifndef IMAGE_HEADER
#define IMAGE_HEADER

#include <stdint.h>
#include <stdio.h>

struct pixel {
    uint8_t r, g, b;
};

struct image {
    uint64_t width, height;
    struct pixel* pixels;
};

struct image create_image(uint64_t width, uint64_t height);

void delete_image(struct image image);

uint32_t get_image_size(const struct image *img);

#endif
