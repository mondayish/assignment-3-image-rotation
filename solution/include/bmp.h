#ifndef BMP_HEADER
#define BMP_HEADER

#include "../include/image.h"
#include  <stdint.h>

#define READ_STATUS_COUNT 5
#define WRITE_STATUS_COUNT 3

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_CONTENT
};

enum read_status from_bmp(FILE *in, struct image *image);

enum write_status {
    WRITE_OK = 0,
    WRITE_INVALID_HEADER,
    WRITE_INVALID_CONTENT
};

enum write_status to_bmp(FILE *out, struct image *image);

#endif
