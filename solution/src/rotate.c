#include "../include/rotate.h"
#include <malloc.h>

void set_pixel(struct image *image, const struct pixel pixel, uint64_t row, uint64_t column) {
    image->pixels[row * image->width + column] = pixel;
}

struct pixel get_pixel(const struct image *image, uint64_t row, uint64_t column) {
    return image->pixels[row * image->width + column];
}

struct image rotate(struct image *source) {
    struct image result = create_image(source->height, source->width);
    result.pixels = malloc(sizeof(struct pixel) * source->height * source->width);
    for (size_t i = 0; i < source->width; i++) {
        for (size_t j = 0; j < source->height; j++) {
            struct pixel pixel = get_pixel(source, j, i);
            set_pixel(&result, pixel, i, result.width - 1 - j);
        }
    }
    return result;
}
