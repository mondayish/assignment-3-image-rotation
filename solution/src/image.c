#include "image.h"
#include "stdlib.h"

struct image create_image(uint64_t width, uint64_t height) {
    struct image result = {0};
    result.width = width;
    result.height = height;
    return result;
}

void delete_image(struct image image) {
    free(image.pixels);
}

uint32_t get_image_size(const struct image *img) {
    return (sizeof(struct pixel) * img->width + img->width % 4) * img->height;
}
