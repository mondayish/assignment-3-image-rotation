#include "../include/rotate.h"
#include "../include/bmp.h"

static char* read_status_messages[READ_STATUS_COUNT] = {
        [READ_INVALID_SIGNATURE] = "Invalid signature",
        [READ_INVALID_BITS] = "Invalid bits",
        [READ_INVALID_HEADER] = "Invalid header",
        [READ_INVALID_CONTENT] = "Invalid file content"
};

static char* write_status_messages[WRITE_STATUS_COUNT] = {
        [WRITE_INVALID_HEADER] = "Invalid header",
        [WRITE_INVALID_CONTENT] = "Invalid file content"
};

int main(int args, char **argv) {
    if (args < 3) {
        fprintf(stderr, "Program needs 3 arguments: <program_name> <source-image> <transformed-image>");
        return 1;
    }
    FILE *in = NULL;
    FILE *out = NULL;
    char *input_file_name = argv[1];
    char *output_file_name = argv[2];
    struct image source = {0};
    struct image result = {0};

    in = fopen(input_file_name, "rb");
    if (in == NULL) {
        fprintf(stderr, "Error while reading file %s", input_file_name);
        return 1;
    }
    out = fopen(output_file_name, "wb");
    if(out == NULL) {
        fprintf(stderr, "Error while reading file %s", output_file_name);
        return 1;
    }
    
    enum read_status read_status = from_bmp(in, &source);
    if (read_status != READ_OK) {
        fprintf(stderr, "%s", read_status_messages[read_status]);
        delete_image(source);
        return 1;
    }

    result = rotate(&source);
    enum write_status write_status = to_bmp(out, &result);
    if (write_status != WRITE_OK) {
        fprintf(stderr, "%s", write_status_messages[write_status]);
        delete_image(result);
        delete_image(source);
        return 1;
    }

    if (fclose(in) != 0 || fclose(out) != 0) {
        fprintf(stderr, "Error while close files");
        delete_image(result);
        delete_image(source);
        return 1;
    }
    
    delete_image(source);
    delete_image(result);
    return 0;
}
