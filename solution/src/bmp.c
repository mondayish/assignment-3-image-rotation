#include "../include/bmp.h"
#include <malloc.h>
#include <stdio.h>

#define BMP_TYPE 0x4D42
#define BMP_PLANES 1
#define BMP_HEADER_SIZE 40
#define BMP_BUFFER_BIT_COUNT 24

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static enum read_status read_bmp_header(FILE *in, struct bmp_header *header) {
    return fread(header, sizeof(struct bmp_header), 1, in) == 1 ? READ_OK : READ_INVALID_HEADER;
}

static enum read_status read_bmp_content(FILE *in, struct image *image) {
    const uint8_t padding = image->width % 4;
    struct pixel *pixels = malloc(image->width * image->height * sizeof(struct pixel));

    for (size_t i = 0; i < image->height; i++) {
        if (pixels + i * image->width != 0) {
            if (fread(pixels + i * image->width, sizeof(struct pixel), image->width, in) != image->width) {
                free(pixels);
                return READ_INVALID_CONTENT;
            }
            if (fseek(in, padding, SEEK_CUR) != 0) {
                free(pixels);
                return READ_INVALID_CONTENT;
            }
        }
    }
    image->pixels = pixels;
    return READ_OK;
}

static enum write_status write_bmp_header(FILE *out, struct bmp_header *header) {
    return fwrite(header, sizeof(struct bmp_header), 1, out) == 1 ? WRITE_OK : WRITE_INVALID_HEADER;
}

static enum write_status write_bmp_content(FILE *out, struct image *image) {
    const uint8_t padding = image->width % 4;
    const uint8_t paddings[3] = {0};
    for (size_t i = 0; i < image->height; i++) {
        if (fwrite(image->pixels + i * image->width, sizeof(struct pixel) * image->width, 1, out) != 1)
            return WRITE_INVALID_CONTENT;
        if (fwrite(paddings, padding, 1, out) != 1 && padding != 0)
            return WRITE_INVALID_CONTENT;
    }
    return WRITE_OK;
}

static struct bmp_header create_header(struct image *image) {
    const uint32_t image_size = get_image_size(image);
    struct bmp_header header = {
            .bfType = BMP_TYPE,
            .bfileSize = sizeof(struct bmp_header) + image_size,
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = BMP_HEADER_SIZE,
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = BMP_PLANES,
            .biBitCount = BMP_BUFFER_BIT_COUNT,
            .biCompression = 0,
            .biSizeImage = image_size,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0,

    };
    return header;
}

enum read_status from_bmp(FILE *in, struct image *image) {
    struct bmp_header bmp_header = {0};
    enum read_status bmp_header_status = read_bmp_header(in, &bmp_header);
    if (bmp_header_status != READ_OK) {
        return bmp_header_status;
    }
    *image = create_image(bmp_header.biWidth, bmp_header.biHeight);
    return read_bmp_content(in, image);
}

enum write_status to_bmp(FILE *out, struct image *image) {
    struct bmp_header bmp_header = create_header(image);
    enum write_status bmp_header_status = write_bmp_header(out, &bmp_header);
    if (bmp_header_status != WRITE_OK) {
        return bmp_header_status;
    }
    return write_bmp_content(out, image);
}
